package main

import (
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"
)

type Request struct {
	Url string `json:"url,omitempty"`
}

type Response struct {
	StatusCode int               `json:"statusCode,omitempty"`
	Headers    map[string]string `json:"headers,omitempty"`
	Body       string            `json:"body,omitempty"`
}

func main() {
	port := 8080
	http.HandleFunc("/images/fetch", resizeImageHandler)

	fmt.Printf("Server started on localhost:%d\n", port)
	_ = http.ListenAndServe(fmt.Sprintf(":%d", port), nil)
}

func fetchImage(urlString string) ([]byte, error) {
	parsedURL, err := url.Parse(urlString)
	if err != nil {
		return nil, err
	}

	response, err := http.Get(parsedURL.String())
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("Failed to fetch the image. Status code: %d", response.StatusCode)
	}

	return io.ReadAll(response.Body)
}

func GetImageType(contentType string) (string, error) {
	types := strings.Split(contentType, ";")

	for _, t := range types {
		t := strings.Trim(t, " ")
		if strings.Contains(t, "image") {
			return t, nil
		}
	}

	return "", errors.New("Content-Type: " + contentType + "is invalid.")
}

func RedirectTo(url string) (*Response, error) {
	headers := make(map[string]string)
	headers["Location"] = url

	return &Response{
		StatusCode: http.StatusFound,
		Headers:    headers,
	}, nil
}

func ResponseNotFound(error string) (*Response, error) {
	return &Response{
		StatusCode: http.StatusNotFound,
		Body:       error,
	}, nil
}

func resizeImageHandler(res http.ResponseWriter, req *http.Request) {
	imageURL := req.URL.Query().Get("url")

	imageData, err := fetchImage(imageURL)
	if err != nil {
		fmt.Println("Error reading image data:", err)
        return
	}

	contentType := http.DetectContentType(imageData)

	res.Header().Set("Content-Type", contentType)
	res.WriteHeader(200)
	res.Write(imageData)
	req.Body.Close()
}
